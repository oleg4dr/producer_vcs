/*
 Navicat Premium Data Transfer

 Source Server         : PostgreSQL
 Source Server Type    : PostgreSQL
 Source Server Version : 120000
 Source Host           : localhost:5432
 Source Catalog        : postgres
 Source Schema         : NotNormalizedClients

 Target Server Type    : PostgreSQL
 Target Server Version : 120000
 File Encoding         : 65001

 Date: 27/02/2020 03:25:01
*/


-- ----------------------------
-- Table structure for Composition
-- ----------------------------
DROP TABLE IF EXISTS "NotNormalizedClients"."Composition";
CREATE TABLE "NotNormalizedClients"."Composition" (
  "userId" uuid NOT NULL,
  "username" varchar(255) COLLATE "pg_catalog"."default",
  "email" varchar(255) COLLATE "pg_catalog"."default",
  "messageId" uuid NOT NULL,
  "forumId" uuid NOT NULL,
  "body" varchar(255) COLLATE "pg_catalog"."default",
  "subject" varchar(255) COLLATE "pg_catalog"."default",
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "themeId" uuid,
  "roleId" uuid,
  "canEdit" bool,
  "canDelete" bool,
  "canPost" bool,
  "font" varchar(255) COLLATE "pg_catalog"."default",
  "language" varchar(255) COLLATE "pg_catalog"."default",
  "color" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of Composition
-- ----------------------------
INSERT INTO "NotNormalizedClients"."Composition" VALUES ('02f0de36-5dba-4d9b-9bba-7c8fc91ae421', 'username', 'user@name.com', 'cc4f7d7f-2a52-4122-8d43-dc9982f0882f', 'b2f0a4b0-d19d-46d5-b519-57360044538e', 'body of some msg', 'subject of some msg', 'name of some forum', '91f2e1c9-e374-4e75-b0fc-6f8202616576', 'd89695aa-7e46-42cf-b687-b4ac91eab076', 't', 'f', 't', 'some font', 'English', 'black');

-- ----------------------------
-- Primary Key structure for table Composition
-- ----------------------------
ALTER TABLE "NotNormalizedClients"."Composition" ADD CONSTRAINT "Composition_pkey" PRIMARY KEY ("userId", "messageId", "forumId");
