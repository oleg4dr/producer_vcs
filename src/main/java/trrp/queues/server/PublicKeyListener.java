package trrp.queues.server;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;

@Component
public class PublicKeyListener {

    private Queue AESKeyQueue = new Queue("AESKey");

    private final ServiceImplementation serviceImplementation;
    private final RabbitTemplate template;

    public PublicKeyListener(RabbitTemplate template, ServiceImplementation serviceImplementation) {
        this.template = template;
        this.serviceImplementation = serviceImplementation;
    }

    @RabbitListener(queues = "publickey")
    public void receiveKeyEncryptMsgAndSendIt(PublicKey key) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {
        //записываем в файл  полученный ключ
        File f = new File("KeyPair/publicKey");
        f.getParentFile().mkdirs();
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(key.getEncoded());
        fos.flush();
        System.out.println("Приложение получило новый публичный ключ.\nОтправляю симметричный ключ зашифрованный полученным публичным ключем");
        this.template.convertAndSend(AESKeyQueue.getName(), serviceImplementation.encryptAESKey(key));
    }
}
