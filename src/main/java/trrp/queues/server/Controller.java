package trrp.queues.server;

import org.springframework.web.bind.annotation.*;

import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping(value = "/mqapp")
public class Controller {

    private final
    ServiceImplementation serviceImplementation;

    public Controller(ServiceImplementation serviceImplementation) {
        this.serviceImplementation = serviceImplementation;
    }

    @PostMapping(value = "/postrsamsg")
    public String postRSAMsg() {
        serviceImplementation.sendRSAMsg();
        return "message encrypted with symmetric key known only to app was sent to service";
    }

}
