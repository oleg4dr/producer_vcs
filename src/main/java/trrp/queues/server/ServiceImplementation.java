package trrp.queues.server;

import model.NonNormalizedMsg;
import org.jooq.DSLContext;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import util.AsymmetricCryptography;
import util.MyConverter;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.UUID;

import static database.Tables.*;

@Service
public class ServiceImplementation {

    private Queue RSAQueue = new Queue("RSA");

    @Value("${encryption.AESKey}")
    private byte[] AESKey;

    private final RabbitTemplate template;
    private final DSLContext create;

    public ServiceImplementation(DSLContext create, RabbitTemplate template) {
        this.create = create;
        this.template = template;
    }

    byte[] encryptAESKey(PublicKey key) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, IllegalBlockSizeException {
        AsymmetricCryptography ac = new AsymmetricCryptography();
        return ac.encryptMsg(AESKey, key);
    }

    void sendRSAMsg() {
        byte[] msg =  encryptMsg(genSaveSelect(), AESKey);
        //отправим в очередь RSA сообщение зашифрованное симметричным ключем шифрования (который был отправлен сервису в шифрованном виде)
        this.template.convertAndSend(RSAQueue.getName(), msg);
        System.out.println("Отправлено сообщение в очередь RSA");
    }

    //шифрует сообщение симметричным ключом AES
    private byte[] encryptMsg(NonNormalizedMsg msg, byte[] symmetricKey)  {
        Cipher c = null;
        try {
            c = Cipher.getInstance("AES");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
        SecretKeySpec k = new SecretKeySpec(symmetricKey, "AES");
        try {
            assert c != null;
            c.init(Cipher.ENCRYPT_MODE, k);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        try {
            return c.doFinal(MyConverter.serialize(msg));
        } catch (IllegalBlockSizeException | BadPaddingException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //создает записть в бд с новыми id н возвращает ее как объект
    private NonNormalizedMsg genSaveSelect(){
        //лень наполнять бд, поэтому просто меняю ключевые поля
        NonNormalizedMsg msg = create.select().from(COMPOSITION).fetchAny().into(NonNormalizedMsg.class);
        msg.setForumId(UUID.randomUUID());
        msg.setUserId(UUID.randomUUID());
        msg.setMessageId(UUID.randomUUID());
        msg.setRoleId(UUID.randomUUID());
        msg.setThemeId(UUID.randomUUID());
        //сохраняю в бд
        create.newRecord(COMPOSITION, msg).store();
        //для соблюдения условий ЛР будем доставать из БД запись а не отправлять объект из буфера
        return create.select().from(COMPOSITION).where(COMPOSITION.MESSAGEID.eq(msg.getMessageId())).fetchOne().into(NonNormalizedMsg.class);
    }

}
